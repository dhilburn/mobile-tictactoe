﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TicTacToeController : MonoBehaviour
{

    // Holds game board squares
    public SQUARE_STATE[] board = new SQUARE_STATE[9];

    // whose turn is it?
    public bool xTurn;
    // playing single player or multiplayer?
    public bool singlePlayer;

    public Text turnIndicator_Landscape;
    public Text turnIndicator_Portrait;

    // Store UI Game Objects
    public GameObject[] buttons_Landscape;
    public GameObject[] buttons_Portrait;
    public Image[] squares_Landscape;
    public Image[] squares_Portrait;
    public Text[] squaresText_Landscape;
    public Text[] squaresText_Portrait;

    // Store images for X and O
    public Sprite oImage;
    public Sprite xImage;

    // Store each board
    public GameObject gameBoard_Landscape;
    public GameObject gameBoard_Portrait;

    public GameObject mainMenu;
    [SerializeField]
    GameObject newGameButton;
    [SerializeField]
    GameObject singlePlayerButton;
    [SerializeField]
    GameObject multiplayerButton;

    public GameObject coinMenu;
    [SerializeField]
    Text coinMessage;
    [SerializeField]
    Text coin;
    [SerializeField]
    GameObject flipCoinButton;
    [SerializeField]
    GameObject gameStartbutton;

    public GameObject gameOverMenu;
    public Text gameOverText;

    public AudioClip gameMusic;
    public AudioClip menuMusic;

    public AudioClip xPlace;
    public AudioClip oPlace;
    public AudioClip coinToss;
    public AudioClip gameEnd;
    public AudioClip button;

    AudioSource soundSource;
    AudioSource musicSource;

    void Start()
    {
        mainMenu.SetActive(true);
        coinMenu.SetActive(false);
        gameBoard_Landscape.SetActive(false);
        gameBoard_Portrait.SetActive(false);
        gameOverMenu.SetActive(false);

        soundSource = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>();
        musicSource = GameObject.Find("Director").GetComponent<AudioSource>();

        musicSource.clip = menuMusic;
        musicSource.volume = 0.7f;
        musicSource.Play();
    }

    void Update()
    {
        if (!(mainMenu.activeInHierarchy || gameOverMenu.activeInHierarchy || coinMenu.activeInHierarchy))
        {
            musicSource.clip = gameMusic;
            RotateBoard();
            if (singlePlayer && !xTurn)
            {
                ComputerPlay();
            }
        }
        else
        {
            musicSource.clip = menuMusic;
        }

        if (!musicSource.isPlaying)
            musicSource.Play();
    }


    public void _ShowCoinMenu(bool gameType)
    {
        soundSource.PlayOneShot(button);
        singlePlayer = gameType;
        mainMenu.SetActive(false);
        coinMenu.SetActive(true);
    }

    public void _CoinFlip()
    {
        if(flipCoinButton.GetComponent<Button>())
        {
            soundSource.PlayOneShot(coinToss);
            int starter = Random.Range(0, 100);
            if (starter > 50f)
                xTurn = true;
            else
                xTurn = false;

            Debug.Log("Starter value: " + starter);

            CoinResult();

            gameStartbutton.SetActive(true);
        }
    }

    void CoinResult()
    {
        if (singlePlayer)
        {
            if (xTurn)
            {
                coinMessage.text = "You go first!";
                coin.text = "X";
            }
            else
            {
                coinMessage.text = "The computer goes first!";
                coin.text = "O";
            }
        }
        else
        {
            if (xTurn)
            {
                coinMessage.text = "X goes first!";
                coin.text = "X";
            }
            else
            {
                coinMessage.text = "O goes first!";
                coin.text = "O";
            }
        }
    }
    
    public void _TransitionToNewGame()
    {

        coinMessage.text = "Who goes first?";
        coin.text = "";
        flipCoinButton.SetActive(true);
        gameStartbutton.SetActive(false);
        coinMenu.SetActive(false);
        _NewGame();
    }

    public void _NewGame()
    {
        board = new SQUARE_STATE[9];

        for (int i = 0; i < 9; i++)
        {
            buttons_Landscape[i].SetActive(true);
            buttons_Portrait[i].SetActive(true);
            squares_Landscape[i].gameObject.SetActive(false);
            squares_Portrait[i].gameObject.SetActive(false);
            squaresText_Landscape[i].text = "";
            squaresText_Portrait[i].text = "";
        }
        
        gameBoard_Landscape.SetActive(true);
        if (xTurn)
        {
            turnIndicator_Landscape.text = "X's Turn!";
            turnIndicator_Portrait.text = "X's Turn!";
        }
        else
        {
            turnIndicator_Landscape.text = "O's Turn!";
            turnIndicator_Portrait.text = "O's Turn!";
        }
    }

    public void _SquareClicked(int squareIndex)
    {
        buttons_Landscape[squareIndex].SetActive(false);
        buttons_Portrait[squareIndex].SetActive(false);
        squares_Landscape[squareIndex].gameObject.SetActive(true);
        squares_Portrait[squareIndex].gameObject.SetActive(true);

        if (singlePlayer)
        {
            if (xTurn)
                SinglePlayerPlay(squareIndex);     
        }
        else
        {
            ChangeTurns(squareIndex);
        }
        CheckVictory();
    }

    void SinglePlayerPlay(int squareIndex)
    {
        soundSource.PlayOneShot(xPlace);
        // Place an X on the board
        squares_Landscape[squareIndex].sprite = xImage;
        squaresText_Landscape[squareIndex].text = "X";

        squares_Portrait[squareIndex].sprite = xImage;
        squaresText_Portrait[squareIndex].text = "X";

        // Inform the game that this square belongs to X
        board[squareIndex] = SQUARE_STATE.X_CONTROL;
        // Change to O's turn
        xTurn = false;
        // Change the text to show it is O's turn
        turnIndicator_Landscape.text = "O's turn!";
        turnIndicator_Portrait.text = "O's turn!";

        Debug.Log("X placed on square " + squareIndex);
    }

    void ComputerPlay()
    {
        int index = ComputerMove();

        soundSource.PlayOneShot(oPlace);
        buttons_Landscape[index].SetActive(false);
        buttons_Portrait[index].SetActive(false);
        squares_Landscape[index].gameObject.SetActive(true);
        squares_Portrait[index].gameObject.SetActive(true);
        squares_Landscape[index].sprite = oImage;
        squares_Portrait[index].sprite = oImage;
        squaresText_Landscape[index].text = "O";
        squaresText_Portrait[index].text = "O";

        board[index] = SQUARE_STATE.O_CONTROL;

        xTurn = true;

        turnIndicator_Landscape.text = "X's turn!";
        turnIndicator_Portrait.text = "X's turn!";

        Debug.Log("O placed on square " + index);

        CheckVictory();
    }

    int ComputerMove()
    {
        int index;
        int canWin = WinIndex(SQUARE_STATE.O_CONTROL);
        int defend = WinIndex(SQUARE_STATE.X_CONTROL);

        if (canWin != -1)
        {
            index = canWin;
        }
        else if (defend != -1)
        {
            index = defend;
        }
        else
        {
            do
            {
                index = Random.Range(0, 9);
            } while (board[index] != SQUARE_STATE.CLEAR);
        }
        return index;
    }

    int WinIndex(SQUARE_STATE winningState)
    {
        WINNING winningPlay = CheckForWin(winningState);

        int win = -1;

        switch (winningPlay)
        {
            case(WINNING.ROW_1):
                if (board[0] == SQUARE_STATE.CLEAR)
                    win = 0;
                else if (board[1] == SQUARE_STATE.CLEAR)
                    win = 1;
                else if(board[2] == SQUARE_STATE.CLEAR)
                    win = 2;
                break;
            case(WINNING.ROW_2):
                if (board[3] == SQUARE_STATE.CLEAR)
                    win = 3;
                else if (board[4] == SQUARE_STATE.CLEAR)
                    win = 4;
                else if (board[5] == SQUARE_STATE.CLEAR)
                    win = 5;
                break;
            case(WINNING.ROW_3):
                if (board[6] == SQUARE_STATE.CLEAR)
                    win = 6;
                else if (board[7] == SQUARE_STATE.CLEAR)
                   win = 7;
                else if (board[8] == SQUARE_STATE.CLEAR)
                    win = 8;
                break;
            case(WINNING.COL_1):
                if (board[0] == SQUARE_STATE.CLEAR)
                    win = 0;
                else if (board[3] == SQUARE_STATE.CLEAR)
                    win = 3;
                else if(board[6] == SQUARE_STATE.CLEAR)
                    win = 6;
                break;
            case(WINNING.COL_2):
                if (board[1] == SQUARE_STATE.CLEAR)
                    win = 1;
                else if (board[4] == SQUARE_STATE.CLEAR)
                    win = 4;
                else if (board[7] == SQUARE_STATE.CLEAR)
                    win = 7;
                break;
            case(WINNING.COL_3):
                if (board[2] == SQUARE_STATE.CLEAR)
                    win = 2;
                else if (board[5] == SQUARE_STATE.CLEAR)
                    win = 5;
                else if (board[8] == SQUARE_STATE.CLEAR)
                    win = 8;
                break;
            case(WINNING.DIAG_1):
                if (board[0] == SQUARE_STATE.CLEAR)
                    win = 0;
                else if (board[4] == SQUARE_STATE.CLEAR)
                    win = 4;
                else if (board[8] == SQUARE_STATE.CLEAR)
                    win = 8;
                break;
            case(WINNING.DIAG_2):
                if (board[2] == SQUARE_STATE.CLEAR)
                    win = 2;
                else if (board[4] == SQUARE_STATE.CLEAR)
                    win = 4;
                else if (board[6] == SQUARE_STATE.CLEAR)
                    win = 6;
                break;
            default:
                win = -1;
                break;
        }
        return win;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns> 0 for no victory available
    ///           1 for victory in top row
    ///           2 for victory in middle row
    ///           3 for victory in bottom row
    ///           4 for victory in left column
    ///           5 for victory in middle column
    ///           6 for victory in right column
    ///           7 for tl->br diag
    ///           8 for tr->bl diag
    /// </returns>
    WINNING CheckForWin(SQUARE_STATE winningState)
    {
        int row1 = 0; // top row
        int row2 = 0; // mid row
        int row3 = 0; // bottom row
        int col1 = 0; // left col
        int col2 = 0; // mid col
        int col3 = 0; // right col
        int diag1 = 0;// top-left to bottom-right diag
        int diag2 = 0;// top-right to bottom-left diag

        if (board[0] == winningState)
        {
            row1++;
            col1++;
            diag1++;
        }
        if (board[1] == winningState)
        {
            row1++;
            col2++;
        }
        if (board[2] == winningState)
        {
            row1++;
            col3++;
            diag2++;
        }
        if (board[3] == winningState)
        {
            row2++;
            col1++;
        }
        if (board[4] == winningState)
        {
            row2++;
            col2++;
            diag1++;
            diag2++;
        }
        if (board[5] == winningState)
        {
            row2++;
            col3++;
        }
        if (board[6] == winningState)
        {
            row3++;
            col1++;
            diag2++;
        }
        if (board[7] == winningState)
        {
            row3++;
            col2++;
        }
        if (board[8] == winningState)
        {
            row3++;
            col3++;
            diag1++;
        }

        if (row1 > 1)
            return WINNING.ROW_1;
        else if (row2 > 1)
            return WINNING.ROW_2;
        else if (row3 > 1)
            return WINNING.ROW_3;
        else if (col1 > 1)
            return WINNING.COL_1;
        else if (col2 > 1)
            return WINNING.COL_2;
        else if (col3 > 1)
            return WINNING.COL_3;
        else if (diag1 > 1)
            return WINNING.DIAG_1;
        else if (diag2 > 1)
            return WINNING.DIAG_2;
        else
            return WINNING.NO_WIN;
    }


    public void CheckVictory()
    {
        for (int i = 0; i < 3; i++)
        {
            // Check columns      
            if (board[i] != SQUARE_STATE.CLEAR && board[i] == board[i + 3] && board[i] == board[i + 6])
            {
                SetWinner(board[i]);
                return;
            }
            // Check rows
            else if (board[i * 3] != SQUARE_STATE.CLEAR && board[i * 3] == board[(i * 3) + 1] && board[i * 3] == board[(i * 3) + 2])
            {
                SetWinner(board[i * 3]);
                return;
            }
        }
        if (board[0] != SQUARE_STATE.CLEAR && board[0] == board[4] && board[0] == board[8])
        {
            SetWinner(board[0]);
            return;
        }
        else if (board[2] != SQUARE_STATE.CLEAR && board[2] == board[4] && board[2] == board[6])
        {
            SetWinner(board[2]);
            return;
        }

        for (int i = 0; i < 9; i++)
        {
            if (board[i] == SQUARE_STATE.CLEAR)
                return;
        }

        SetWinner(SQUARE_STATE.CLEAR);
    }

    void SetWinner(SQUARE_STATE winner)
    {
        gameBoard_Landscape.gameObject.SetActive(false);
        gameBoard_Portrait.gameObject.SetActive(false);
        gameOverMenu.gameObject.SetActive(true);

        if (winner == SQUARE_STATE.X_CONTROL)
            gameOverText.text = "X Wins!";
        else if (winner == SQUARE_STATE.O_CONTROL)
            gameOverText.text = "O Wins!";
        else
            gameOverText.text = "Draw game!";
    }

    public void _BackToMain()
    {
        soundSource.PlayOneShot(button);
        if (gameBoard_Landscape.activeInHierarchy)
            gameBoard_Landscape.SetActive(false);
        if (gameBoard_Portrait.activeInHierarchy)
            gameBoard_Portrait.SetActive(false);
        gameOverMenu.SetActive(false);
        mainMenu.SetActive(true);
    }    

    public void QuitGame()
    {
        Application.Quit();
    }    

    void ChangeTurns(int squareIndex)
    {
        if (xTurn)
        {
            soundSource.PlayOneShot(xPlace);
            // Place an X on the board
            squares_Landscape[squareIndex].sprite = xImage;
            squaresText_Landscape[squareIndex].text = "X";

            squares_Portrait[squareIndex].sprite = xImage;
            squaresText_Portrait[squareIndex].text = "X";

            // Inform the game that this square belongs to X
            board[squareIndex] = SQUARE_STATE.X_CONTROL;
            // Change to O's turn
            xTurn = false;
            // Change the text to show it is O's turn
            turnIndicator_Landscape.text = "O's turn!";
            turnIndicator_Portrait.text = "O's turn!";

            Debug.Log("X placed on square " + squareIndex);
        }
        else
        {
            soundSource.PlayOneShot(oPlace);
            // Place an O on the board
            squares_Landscape[squareIndex].sprite = oImage;
            squaresText_Landscape[squareIndex].text = "O";

            squares_Portrait[squareIndex].sprite = oImage;
            squaresText_Portrait[squareIndex].text = "O";

            // Inform the game that this square belongs to O
            board[squareIndex] = SQUARE_STATE.O_CONTROL;
            // Change to X's turn
            xTurn = true;
            // Change the text to show it is X's turn
            turnIndicator_Landscape.text = "X's turn!";
            turnIndicator_Portrait.text = "X's turn!";
            Debug.Log("O placed on square " + squareIndex);
        }
    }    

    void RotateBoard()
    {
        if (Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
        {
            gameBoard_Landscape.SetActive(false);
            gameBoard_Portrait.SetActive(true);
        }
        else if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight)
        {
            gameBoard_Portrait.SetActive(false);
            gameBoard_Landscape.SetActive(true);
        }
    }
}
