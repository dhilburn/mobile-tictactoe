﻿/// <summary>
/// Inform the controller who is controlling a square.
/// </summary>
public enum SQUARE_STATE
{
    CLEAR,
    X_CONTROL,
    O_CONTROL
}

public enum WINNING
{
    NO_WIN,
    ROW_1,
    ROW_2,
    ROW_3,
    COL_1,
    COL_2,
    COL_3,
    DIAG_1,
    DIAG_2
}
